#ifndef _FILE_LIST_TREE_H_
#define _FILE_LIST_TREE_H_

#include "mtd.h"

#include "yaffs_list.h"
#include "yaffs_guts.h"

#include "tree.h"


struct file_trunk{
	int trunk_number; /* the logical position on the NAND */
	int num_headers_before; /* the number of obj headers before this chunk */

	struct yaffs_ext_tags tags; /* should be read by yaffs_read_chunk_with_tags */
	struct list_head list_entry; /* list sorted by chunk id */

	RB_ENTRY(file_trunk) rb_entry; /* rb-tree is used to deduplicate the chunk_id and sort the trunks by thunk_id */
};

struct file_trunk_list{
	unsigned obj_id; /* yaffs object id */

	//int place_holder; /* version-independed file meta-data */
	enum yaffs_obj_type obj_type; /* object type (dir / file) */

	RB_ENTRY(file_trunk_list) rb_entry;  /* BSD rb-tree entry, indexed by obj_id */
	struct list_head chunk_list; /* list of file chunks, sorted by seq_number (chronically) */
	struct file_trunk_list *parent_object; /* obj_id of parent directory, usually */

};

RB_HEAD(file_list_tree_t, file_trunk_list);

struct file_list_tree_t *build_file_list_tree(struct mtd_info *mtd);

void file_list_tree_iterate(struct mtd_info *mtd, struct file_list_tree_t* tree, int (*func)(struct mtd_info *, struct file_trunk_list *)) ;

#endif
