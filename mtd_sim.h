#ifndef _MTD_SIM_INIT_H_
#define _MTD_SIM_INIT_H_

#include "mtd.h"
extern unsigned int chunkSize;
extern unsigned int spareSize;
extern unsigned int chunksPerBlock;
extern unsigned int totalBytesPerBlock;
extern unsigned int totalBytesPerChunk;
extern unsigned int dataBytesPerBlock;
extern unsigned int totalBlocks;
extern unsigned int totalChunks;
extern unsigned int totalSize;


int mtd_sim_init(struct mtd_info *, char *);

#endif
