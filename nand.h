#ifndef _NAND_H_
#define _NAND_H_

int nand_init(struct mtd_info *);
int nand_read_img(int, int);
#endif
