#ifndef _YAFFS_READ_H_
#define _YAFFS_READ_H_

#include "mtd.h"
#include "yaffs_packedtags2.h"

int yaffs_read_chunk_with_tags(struct mtd_info *mtd, int nand_chunk, uint8_t *data, struct yaffs_ext_tags *tags );
#endif
