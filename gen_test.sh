#!/bin/bash

# $1: n chunks, $2: m hdr chunks $3: file

chunk_size=4096
n_chunks=$1
n_hdrs=$2
n_datachunks=$(($n_chunks - $n_hdrs))
n_versions=$(($n_hdrs / 2))
#n_versions=$n_hdrs
n_datachunks_per=$(($n_datachunks / $n_hdrs))
echo $n_datachunks, $n_datachunks_per

for i in `seq 0 $(($n_versions - 1))` ; do
	dd if=/dev/urandom of="$3" bs="$(($chunk_size * $n_datachunks_per))" \
		count=1 seek="$i" 2> /dev/null
	echo $i
done
