# Makefile for yaffs2recover
# adapted from Makefile for mkyaffs
#
# NB this is not yet suitable for putting into the kernel tree.
# YAFFS: Yet another Flash File System. A NAND-flash specific file system. 
#
# Copyright (C) 2002 Aleph One Ltd.
#   for Toby Churchill Ltd and Brightstar Engineering
#
# Created by Charles Manning <charles@aleph1.co.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.

## Change or override  KERNELDIR to your kernel

#KERNELDIR = /usr/src/kernel-headers-2.4.18

CFLAGS =    -g -Wall -DCONFIG_YAFFS_UTIL
CFLAGS+=   -Wshadow -Wpointer-arith -Wwrite-strings -Wstrict-prototypes 
CFLAGS+=   -Wredundant-decls -Wnested-externs -Winline

## Change if you are using a cross-compiler
MAKETOOLS = 

CC=$(MAKETOOLS)gcc

COMMON_BASE_C_LINKS = yaffs_ecc.c yaffs_packedtags2.c
COMMON_BASE_LINKS = $(COMMON_BASE_C_LINKS) yaffs_ecc.h yaffs_guts.h yaffs_packedtags2.h yaffs_trace.h yaffs_yaffs2.h
COMMON_DIRECT_C_LINKS = yaffs_hweight.c
COMMON_C_LINKS = $(COMMON_DIRECT_C_LINKS) $(COMMON_BASE_C_LINKS)
COMMON_DIRECT_LINKS= $(COMMON_DIRECT_C_LINKS) yportenv.h yaffs_hweight.h yaffs_list.h
COMMONOBJS = $(COMMON_C_LINKS:.c=.o)

YAFFS2RECOVERSOURCES = yaffs2recover.c nand.c mtd_sim.c yaffs_read.c file_list_tree.c file_recover.c interactive.c
YAFFS2RECOVEROBJS = $(YAFFS2RECOVERSOURCES:.c=.o)
#YAFFS2RECOVERLINKS = yaffs_packedtags2.c
YAFFS2RECOVERUTILSLINKS = yutilsenv.h


BASE_LINKS = $(YAFFS2RECOVERLINKS) $(MKYAFFS2LINKS) $(COMMON_BASE_LINKS)
DIRECT_LINKS = $(COMMON_DIRECT_LINKS)
UTILS_LINKS = $(YAFFS2RECOVERUTILSLINKS)
ALL_LINKS = $(BASE_LINKS) $(DIRECT_LINKS) $(UTILS_LINKS)

all: yaffs2recover

$(BASE_LINKS):
	ln -s yaffs2/$@ $@

$(DIRECT_LINKS):
	ln -s yaffs2/direct/$@ $@

$(UTILS_LINKS):
	ln -s yaffs2/utils/$@ $@

$(COMMONOBJS) $(YAFFS2RECOVEROBJS) : $(ALL_LINKS)

$(COMMONOBJS) $(YAFFS2RECOVEROBJS) : %.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

cscope:
	cscope -Rbq

yaffs2recover: $(YAFFS2RECOVEROBJS) $(COMMONOBJS)
	$(CC) -o $@ $^


clean:
	rm -f $(COMMONOBJS)  $(YAFFS2RECOVEROBJS) $(ALL_LINKS) yaffs2recover
