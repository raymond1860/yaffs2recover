#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include <mtd/mtd-abi.h>

struct nand_oobinfo oobinfo;

int main()
{
	int r,i,fd;
	fd = open("/dev/mtd0", O_RDONLY);
	r = ioctl(fd, MEMGETOOBSEL, &oobinfo);
	if (r < 0)
	{
		printf("Error: %e\n",r);
		return r;
	}
	printf("ECC BYTES: %d", oobinfo.eccbytes);
	printf("ECC POS: ");
	for (i = 0; i < oobinfo.eccbytes; i++)
		printf("%d ", oobinfo.eccpos[i]);
	printf("\n");
	printf("OOB FREE: ");
	printf("offset: %d", oobinfo.oobfree[0][0]);
	printf("length: %d", oobinfo.oobfree[0][1]);
	return 0;
}
