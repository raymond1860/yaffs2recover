#ifndef _FILE_RECOVER_H_
#define _FILE_RECOVER_H_

#include "file_list_tree.h"
#include "mtd.h"

int file_recover_iterate_tree(struct mtd_info *mtd, struct file_list_tree_t *ptree);

#endif

